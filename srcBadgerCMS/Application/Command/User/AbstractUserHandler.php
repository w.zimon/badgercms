<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\User;

use BadgerCMS\Domain\User\Repository\UserRepositoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

abstract class AbstractUserHandler
{

    protected UserRepositoryInterface $userRepository;
    protected UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserRepositoryInterface $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

}