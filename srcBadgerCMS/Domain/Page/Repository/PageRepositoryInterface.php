<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Page\Repository;

use BadgerCMS\Domain\Page\Page;

/**
 * PageRepository.phputhor Wojciech Zimoń <wojciechzimon93@gmail.com>
 * @package CMS\Domain\Page\Repository\PageRepository
 **/
interface PageRepositoryInterface
{

    public function getAll(string $menuId = null): ?array;

    public function getForAdminEdit(string $id): array;

    public function getOneBySlug(string $slug): ?Page;

    public function delete(string $id): void;
}