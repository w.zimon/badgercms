<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Security;

use BadgerCMS\Domain\User\Exception\AccountBlockedException;
use BadgerCMS\Domain\User\Exception\AccountDeletedException;
use BadgerCMS\Domain\User\Exception\AccountInactiveException;
use BadgerCMS\Domain\User\ValueObject\AuthVO;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserChecker
 *
 * @author  : Wojciech Zimoń <w.zimon@inis.pl>
 * @package CMS\Infrastructure\User\Auth
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * @param UserInterface $user
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AuthVO) {
            return;
        }

        // user is deleted, show a generic Account Not Found message.
        if ($user->isRemoved()) {
            throw new AccountDeletedException('This account don\'t exist.');
        }
        // user account is not active
        if (!$user->isActive()) {
            throw new AccountInactiveException('This account is inactive.');
        }
        // user account is blocked
        if ($user->isBlocked()) {
            throw new AccountBlockedException('This account is blocked.');
        }
    }

    /**
     * @param UserInterface $user
     */
    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof AuthVO) {
            throw new AuthenticationException('This account provide wrong authenticated object.');
        }

    }
}
