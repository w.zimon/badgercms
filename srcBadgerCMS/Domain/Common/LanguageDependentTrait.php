<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Common;

use BadgerCMS\Domain\Language\Language;
use Doctrine\ORM\Mapping as ORM;

trait LanguageDependentTrait
{

    /**
     * @ORM\ManyToOne(targetEntity="BadgerCMS\Domain\Language\Language")
     * @ORM\JoinColumn(name="lang_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $lang;

    /**
     * @return Language
     */
    public function getLang(): Language
    {
        return $this->lang;
    }

    /**
     * @param mixed $lang
     */
    public function setLang($lang): void
    {
        $this->lang = $lang;
    }
}