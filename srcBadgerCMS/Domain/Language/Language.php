<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Language;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BadgerCMS\Infrastructure\Language\LanguageRepository")
 * @ORM\Table(name="langs")
 */
class Language
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="string", length=2)
     * @ORM\GeneratedValue(strategy="NONE")
     */

    private $id;

    /**
     * Language constructor.
     * @param string $lang
     */
    public function __construct(string $lang)
    {
        $this->id = strtolower($lang);
    }

    public function __toString()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

}