<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\PageMenu;

use BadgerCMS\Domain\Page\Repository\MenuRepositoryInterface;

abstract class AbstractMenuHandler
{

    /**
     * @var MenuRepositoryInterface
     */
    protected MenuRepositoryInterface $menuRepository;

    public function __construct(MenuRepositoryInterface $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

}