import Sortable from 'sortablejs/modular/sortable.complete.esm.js';

const routes = require('../../../js/fos_js_routes.json');
import Routing from '../../../bundles/fosjsrouting/js/router.js';

Routing.setRoutingData(routes);

$(document).ready(function () {

    function sort(data) {
        const urlParams = new URLSearchParams(window.location.search);
        data.push({entityId: urlParams.get('entityId')});
        $.post(Routing.generate('admin_menu_ajax_sort'), JSON.stringify(data));
    }

    function walk(root) {
        var lis = $(root).children('ul').children('li');
        var ids = [];
        lis.each(function () {
            ids.push({
                id: $(this).data('id'),
                children: walk(this),
            });
        });
        return ids;
    }

    var nestedSortables = [].slice.call(document.querySelectorAll('#menu-tree .nested-sortable'));
    for (var i = 0; i < nestedSortables.length; i++) {
        new Sortable(nestedSortables[i], {
            group: 'nested',
            animation: 150,
            fallbackOnBody: true,
            // Element dragging ended
            onEnd: function (/**Event*/evt) {
                var itemEl = evt.item;  // dragged HTMLElement
                evt.to;    // target list
                evt.from;  // previous list
                evt.oldIndex;  // element's old index within old parent
                evt.newIndex;  // element's new index within new parent
                evt.oldDraggableIndex; // element's old index within old parent, only counting draggable elements
                evt.newDraggableIndex; // element's new index within new parent, only counting draggable elements
                evt.clone // the clone element
                evt.pullMode;  // when item is in another sortable: `"clone"` if cloning, `true` if moving
                const root = document.getElementById('menu-tree');
                sort(walk(root));
            },
        });
    }
});