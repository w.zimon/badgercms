<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Common;

use Gedmo\Uploadable\MimeType\MimeTypeGuesserInterface;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;

class BadgerMimeTypeGuesser extends FileinfoMimeTypeGuesser implements MimeTypeGuesserInterface
{

    public function guess($filePath)
    {
        return $this->guessMimeType($filePath);
    }
}