<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Page;

use BadgerCMS\Application\Common\LanguageDependentInterface;
use BadgerCMS\Domain\Common\LanguageDependentTrait;
use BadgerCMS\Domain\Common\LifecycleTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;


/**
 * @ORM\Entity(repositoryClass="BadgerCMS\Infrastructure\Page\PageRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="pages", indexes={
 *     @ORM\Index(name="slug_idx", columns={"slug"}),
 *     @ORM\Index(name="active_idx", columns={"active"})
 * })
 */
class Page implements LanguageDependentInterface
{
    use PageModel;
    use LifecycleTrait;
    use LanguageDependentTrait;


    public function __toString()
    {
        return $this->title . '(' . $this->id . ')';
    }

    /**
     * Menu constructor.
     */
    public function __construct()
    {
        $this->menus = new ArrayCollection();
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated(DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return DateTime
     */
    public function getUpdated(): DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated(DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @param Uuid $id
     */
    public function setId(Uuid $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getAccessRole(): string
    {
        return $this->accessRole;
    }

    /**
     * @param string $accessRole
     */
    public function setAccessRole(string $accessRole): void
    {
        $this->accessRole = $accessRole;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        //TODO: refactor
        if (is_array($this->content)){
            return json_encode($this->content);
        }elseif (is_string($this->content)){
            return json_decode($this->content, true);
        }elseif (is_null($this->content)){
            return [];
        }
    }

    /**
     * @param string $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getMenus(): ?Collection
    {
        return $this->menus;
    }

    public function setMenus(?Collection $menus): self
    {
        $this->menus = $menus;

        return $this;
    }

    public function addMenu(Menu $menu): self
    {
        $menu->setPage($this);
        $this->menus->add($menu);

        return $this;
    }


}
