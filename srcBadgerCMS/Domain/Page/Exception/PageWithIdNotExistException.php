<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Page\Exception;

use Exception;
use Throwable;

/**
 * PageWithIdNotExistException.php
 *
 * @author Wojciech Zimoń <wojciechzimon93@gmail.com>
 * @package CMS\Domain\Page\Exception\PageWithIdNotExistException
 **/
class PageWithIdNotExistException extends Exception
{
    const MSG = 'Strona o podanym id nie istnieje (%s)';

    public function __construct(string $id, $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf(self::MSG, $id) , $code, $previous);
    }


}
