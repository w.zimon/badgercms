<?php
declare(strict_types=1);

namespace BadgerCMS\UI\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SfAbstractController;

abstract class AbstractAdminController extends SfAbstractController
{

}