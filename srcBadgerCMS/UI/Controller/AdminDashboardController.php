<?php
declare(strict_types=1);

namespace BadgerCMS\UI\Controller;

use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin2", name="admin_")
 */
class AdminDashboardController extends AbstractAdminController
{

    /**
     * @Route("/", name="dashboard2")
     */
    public function index()
    {
        return $this->render('admin_dashboard/index.html.twig', [
            'controller_name' => 'AdminDashboardController',
        ]);
    }
}
