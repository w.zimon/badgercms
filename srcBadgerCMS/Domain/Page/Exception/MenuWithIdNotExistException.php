<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Page\Exception;

use Exception;
use Throwable;

/**
 * MenuWithIdNotExistException.php
 *
 * @author Wojciech Zimoń <wojciechzimon93@gmail.com>
 * @package CMS\Domain\Page\Exception\MenuWithIdNotExistException
 **/
class MenuWithIdNotExistException extends Exception
{
    const MSG = 'Menu with id: %s don\'t exist';

    public function __construct($id = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf(self::MSG, $id), $code, $previous);
    }

}
