#!/bin/bash

    ./bin/console doctrine:database:create --env=test
    ./bin/console cache:clear --env=test --no-warmup
    ./bin/console doctrine:cache:clear-meta --env=test
    ./bin/console doctrine:cache:clear-query --env=test
    ./bin/console doctrine:cache:clear-result --env=test
    ./bin/console doctrine:migrations:migrate --env=test --no-interaction
    ./bin/console doctrine:fixtures:load --env=test --group=test --no-interaction
