<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Page;

use BadgerCMS\Application\Common\LanguageDependentInterface;
use BadgerCMS\Domain\Common\LanguageDependentTrait;
use BadgerCMS\Domain\Common\LifecycleTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\UuidInterface;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="BadgerCMS\Infrastructure\Page\MenuRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="menus", indexes={
 *     @ORM\Index(name="active_idx", columns={"active"}),
 *     @ORM\Index(name="sort_idx", columns={"sort"}),
 * })
 */
class Menu implements LanguageDependentInterface
{
    use LifecycleTrait;
    use MenuModel;
    use LanguageDependentTrait;


    public static function create(
        UuidInterface $id,
        string $placeholder,
        string $title,
        string $description = '',
        Menu $parent = null
    ) {
        $menu = new self();
        $menu->setId($id);
        $menu->setDescription($description);
        $menu->setPlaceholder($placeholder);
        $menu->setTitle($title);
        $menu->setActive(true);
        $menu->setParent($parent);

        return $menu;
    }

    public function __toString()
    {
        return $this->title . ' (' . $this->id . ')';
    }

    /**
     * Menu constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }


    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated(DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return DateTime
     */
    public function getUpdated(): DateTime
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated(DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @param UuidInterface $id
     */
    public function setId(UuidInterface $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPlaceholder(): string
    {
        return $this->placeholder;
    }

    /**
     * @param string $placeholder
     */
    public function setPlaceholder(string $placeholder): void
    {
        $this->placeholder = $placeholder;
    }


    /**
     * @return string
     */
    public function getRemoteUrl(): ?string
    {
        return $this->remoteUrl;
    }

    /**
     * @param string $remoteUrl
     */
    public function setRemoteUrl(?string $remoteUrl): void
    {
        $this->remoteUrl = $remoteUrl;
    }

    /**
     * @return string
     */
    public function getRemoteUrlTarget(): ?string
    {
        return $this->remoteUrlTarget;
    }

    /**
     * @param string $remoteUrlTarget
     */
    public function setRemoteUrlTarget(string $remoteUrlTarget): void
    {
        $this->remoteUrlTarget = $remoteUrlTarget;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive(bool $active = true): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param mixed $lft
     */
    public function setLft($lft): void
    {
        $this->lft = $lft;
    }

    /**
     * @return mixed
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param mixed $rgt
     */
    public function setRgt($rgt): void
    {
        $this->rgt = $rgt;
    }

    /**
     * @return mixed
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * @param mixed $lvl
     */
    public function setLvl($lvl): void
    {
        $this->lvl = $lvl;
    }

    /**
     * @return string
     */
    public function getSort(): string
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort(string $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param mixed $root
     */
    public function setRoot($root): void
    {
        $this->root = $root;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent(?Menu $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage(Page $page): void
    {
        $this->page = $page;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren() : Collection
    {
        return $this->children;
    }


}
