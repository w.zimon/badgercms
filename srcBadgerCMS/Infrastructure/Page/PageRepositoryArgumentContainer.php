<?php
declare(strict_types=1);

namespace BadgerCMS\Infrastructure\Page;

use BadgerCMS\Application\Services\LanguageService;
use BadgerCMS\Application\Services\TreeGenerator;
use BadgerCMS\Infrastructure\RepositoryArgumentContainer;

abstract class PageRepositoryArgumentContainer extends RepositoryArgumentContainer
{

    private $treeGenerator;

    public function __construct(LanguageService $languageService, TreeGenerator $treeGenerator)
    {
        parent::__construct($languageService);
        $this->treeGenerator = $treeGenerator;
    }

    public function getTreeGenerator(): TreeGenerator
    {
        return $this->treeGenerator;
    }

}