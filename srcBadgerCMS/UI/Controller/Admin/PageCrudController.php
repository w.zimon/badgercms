<?php

namespace BadgerCMS\UI\Controller\Admin;

use BadgerCMS\Application\Command\Page\CreatePage\CreatePageCommand;
use BadgerCMS\Application\Command\Page\EditPage\EditPageCommand;
use BadgerCMS\Application\Command\Page\DeletePage\DeletePageCommand;
use BadgerCMS\Application\Fields\ContentField;
use BadgerCMS\Domain\Page\Page;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Page::class;
    }

    public function createEntity(string $entityFqcn)
    {
        return Page::create(Uuid::uuid4());
    }


    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $cmd = new CreatePageCommand($entityInstance);
        $this->dispatchMessage($cmd);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $cmd = new EditPageCommand($entityInstance);
        $this->dispatchMessage($cmd);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $cmd = new DeletePageCommand($entityInstance);
        $this->dispatchMessage($cmd);
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->update(
            Crud::PAGE_INDEX,
            Action::EDIT,
            fn(Action $action) => $action->setIcon('fa fa-edit')->setLabel(false)
        )
            ->update(
                Crud::PAGE_INDEX,
                Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash')->setLabel(false)
            )
            ->update(
                Crud::PAGE_INDEX,
                Action::NEW,
                fn(Action $action) => $action->setLabel('Dodaj stronę')
            )
            ->add(
                Crud::PAGE_INDEX,
                Action::DETAIL
            )
            ->update(
                Crud::PAGE_INDEX,
                Action::DETAIL,
                fn(Action $action) => $action->setIcon('fa fa-eye')->setLabel(false)
            );

        return parent::configureActions($actions);
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            TextField::new('slug'),
            BooleanField::new('active')
                ->hideOnIndex(),
            ChoiceField::new('access_role')
                ->setChoices(
                    [
                        'Administrator' => 'ROLE_ADMIN',
                        'Moderator'     => 'ROLE_MODERATOR',
                        'Użytkownik'    => 'ROLE_USER',
                    ]
                ),
            ContentField::new('content')->onlyWhenUpdating(),
            TextField::new('type')
        ];
    }

}
