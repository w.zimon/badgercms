<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Security;

use BadgerCMS\Domain\User\Repository\UserRepositoryInterface;
use BadgerCMS\Domain\User\ValueObject\AuthVO;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Description of AuthProvider
 *
 * @author Wojciech Zimoń <wojciechzimon93@gmail.com>
 */
class AuthProvider implements UserProviderInterface
{

    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function loadUserByUsername($username): UserInterface
    {
        return $this->userRepository->getAuth($username);
    }

    public function refreshUser(UserInterface $user): ?UserInterface
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return AuthVO::class;
    }

}
