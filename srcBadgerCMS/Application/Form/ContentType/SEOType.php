<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Form\ContentType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SEOType extends AbstractType
{
    const LABEL = 'Ustawienia SEO';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', TextType::class, [
                'required' => false
            ])
            ->add('metaDescription', TextType::class, [
                'required' => false
            ]);


    }


}