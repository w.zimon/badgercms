<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Security;

use CMS\Domain\User\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * UserVoter.php
 *
 * @author Wojciech Zimoń <wojciechzimon93@gmail.com>
 * @package CMS\Application\Security\UserVoter
 **/
class UserVoter extends Voter
{

    const ADD = 'add';
    const EDIT = 'edit';
    const REMOVE = 'remove';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT, self::REMOVE, self::ADD])) {
            return false;
        }

        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::REMOVE:
            case self::EDIT:
            case self::ADD:
                return $this->canProcess($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canProcess(User $subject, User $user)
    {
        $subjectRole = $subject->getRole();
        $userRole = $user->getRole();

        if(in_array('ROLE_SUPER_ADMIN', $userRole)){
            return true;
        } else if (in_array('ROLE_SUPER_ADMIN', $subjectRole) && $subject->getId() != $user->getId()){
            return false;
        } else {
            return true;
        }
    }

}