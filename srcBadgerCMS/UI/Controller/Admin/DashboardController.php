<?php

namespace BadgerCMS\UI\Controller\Admin;

use BadgerCMS\Domain\Page\Menu;
use BadgerCMS\Domain\Page\Page;
use BadgerCMS\Domain\User\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/{_locale}", name="admin_")
 */
class DashboardController extends AbstractDashboardController
{

    private CrudUrlGenerator $crudUrlGenerator;

    public function __construct(CrudUrlGenerator $crudUrlGenerator)
    {
        $this->crudUrlGenerator = $crudUrlGenerator;
    }

    /**
     * @Route("/", name="dashboard")
     */
    public function index(): Response
    {
        return $this->render('admin_dashboard/index.html.twig', [
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Dashboard');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'ti-dashboard');
        yield MenuItem::linkToCrud('Menu', 'ti-users', Menu::class);
        yield MenuItem::linkToCrud('Page', 'ti-users', Page::class);
        yield MenuItem::linkToCrud('Users', 'ti-users', User::class);
        yield MenuItem::linkToLogout('logout');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        $profileEditUrl = $url = $this->crudUrlGenerator
            ->build()
            ->setController(UserCrudController::class)
            ->setAction(Action::EDIT)
            ->setEntityId($user->getId());
        // Usually it's better to call the parent method because that gives you a
        // user menu with some menu items already created ("sign out", "exit impersonation", etc.)
        // if you prefer to create the user menu from scratch, use: return UserMenu::new()->...
        return parent::configureUserMenu($user)
            // use the given $user object to get the user name
            ->setName($user->getUsername())
            // use this method if you don't want to display the name of the user
            ->displayUserName(false)

            // you can return an URL with the avatar image
            ->setAvatarUrl('https://...')
          //  ->setAvatarUrl($user->getProfileImageUrl())
            // use this method if you don't want to display the user image
            ->displayUserAvatar(false)
            // you can also pass an email address to use gravatar's service
            //->setGravatarEmail($user->getUsername())

            // you can use any type of menu item, except submenus
            ->addMenuItems([
                MenuItem::linkToUrl('My Profile', 'fa fa-id-card', $profileEditUrl),
                MenuItem::section(),
            ]);
    }
}
