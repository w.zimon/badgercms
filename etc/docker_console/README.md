# Docker-console

Docker-console is a tool for manage local DNS with Docker containers IP's

## Usage

```bash
#Run docker-composer up -d -> add containers IP and (hostname from docker-composer.yaml services) to local hosts file
php etc/docker_console/dc.phar up

#Run docker-composer build -> docker-composer up -d -> add containers IP and (hostname from docker-composer.yaml services) to local hosts file
php etc/docker_console/dc.phar rebuild

#Run docker-composer stop
php etc/docker_console/dc.phar stop

#Run docker exec -t -i $containerName bash
php etc/docker_console/dc.phar bash $containerName
```
