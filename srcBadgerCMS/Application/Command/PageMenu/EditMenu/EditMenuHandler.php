<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\PageMenu\EditMenu;

use BadgerCMS\Application\Command\PageMenu\AbstractMenuHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EditMenuHandler extends AbstractMenuHandler implements MessageHandlerInterface
{
    /**
     * @param EditMenuCommand $command
     */
    public function __invoke(EditMenuCommand $command): void
    {
        $this->menuRepository->save($command->getMenu());
    }
}