<?php

namespace BadgerCMS\Application\Common;

use BadgerCMS\Domain\Language\Language;

interface LanguageDependentInterface
{
    public function getLang(): Language;

    public function setLang(Language $lang);
}