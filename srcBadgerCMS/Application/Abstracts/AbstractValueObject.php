<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Abstracts;


/**
 * Class AbstractValueObject
 *
 * @author  : Wojciech Zimoń <w.zimon@inis.pl>
 * @package CMS\Infrastructure\Share
 */
abstract class AbstractValueObject
{

    public function __construct(array $data = [])
    {
        foreach ($data as $property => $value) {
            $property = $this->camelize($property);
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    private function camelize($input, $separator = '_'): string
    {
        return str_replace($separator, '', lcfirst(ucwords($input, $separator)));
    }

}