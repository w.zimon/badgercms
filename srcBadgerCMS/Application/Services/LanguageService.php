<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Services;

use BadgerCMS\Domain\Language\Language;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class LanguageService
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var Language|null
     */
    private $lang;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * LanguageService constructor.
     *
     * @param RequestStack $requestStack
     * @param ParameterBagInterface $parameterBag
     * @param EntityManagerInterface $entityManager
     *
     * @throws Exception
     */
    public function __construct(
        RequestStack $requestStack,
        ParameterBagInterface $parameterBag,
        EntityManagerInterface $entityManager
    ) {
        $this->requestStack = $requestStack;
        $this->parameterBag = $parameterBag;
        $this->entityManager = $entityManager;
       // $this->setLocale();
    }

/*
    private function setLocale(): void
    {
        $request = $this->requestStack->getCurrentRequest();
        if ($request instanceof Request) {
            $this->locale = $request->getLocale();
        } else {
            $this->locale = $this->parameterBag->get('locale');
        }
        $languageEntity = $this->entityManager->getRepository(Language::class)->find($this->locale);
        if ($languageEntity instanceof Language) {
            $this->lang = $languageEntity;
        } else {
            $this->locale = $this->parameterBag->get('locale');
        }
    }*/


    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->entityManager->getRepository(Language::class)->findAll();
    }

}
