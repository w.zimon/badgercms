import '../css/app.scss'

import 'jquery';
import 'popper.js';
import 'jquery.are-you-sure';
import 'bootstrap';
import 'metismenu';
import 'jquery-slimscroll';
import 'jquery.easing';
import 'owl.carousel2';
import './slicknav';
import 'multiple-select';
import './multiple-select-pl-PL';
import './scripts';

import './custom';

global.$ = global.jQuery = $;

