<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\Page\DeletePage;

use BadgerCMS\Application\Command\Page\AbstractPageHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DeletePageHandler extends AbstractPageHandler implements MessageHandlerInterface
{

    /**
     * @param DeletePageCommand $cmd
     */
    public function __invoke(DeletePageCommand $cmd): void
    {
        $this->pageRepository->remove($cmd->getPage());
    }
}
