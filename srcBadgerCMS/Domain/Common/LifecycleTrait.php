<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Common;

use DateTime;

/**
 * LifecycleTrait.php
 *
 * @package CMS\Application\Traits
 * @author  wzimon
 */
trait LifecycleTrait
{

    protected DateTime $created;
    protected DateTime $updated;


    public function setCreated(): void
    {
        if ($this->created === null) {
            $this->created = new DateTime();
        }
    }

    public function setUpdated(): void
    {
        $this->updated = new DateTime();
    }

    public function getCreated(): DateTime
    {
        if ($this->created instanceof \DateTime) {
            return $this->created;
        } else {
            return new \DateTime($this->created);
        }
    }

    public function getUpdated(): DateTime
    {
        if ($this->updated instanceof \DateTime) {
            return $this->updated;
        } else {
            return new \DateTime($this->updated);
        }
    }

}
