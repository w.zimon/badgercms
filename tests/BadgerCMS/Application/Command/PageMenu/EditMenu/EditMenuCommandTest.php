<?php

namespace BadgerCMS\Application\Command\PageMenu\EditMenu;

use App\DataFixtures\TestFixtures;
use BadgerCMS\Domain\Page\Menu;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EditMenuCommandTest extends KernelTestCase
{
    private MessageHandlerInterface $handler;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var \BadgerCMS\Infrastructure\Page\MenuRepository|object|null
     */
    private $repo;


    public function setUp()
    {
        $kernel = self::bootKernel();
        $this->kernel_root = $kernel->getContainer()->getParameter('kernel.project_dir');
        $this->handler = $kernel->getContainer()->get(EditMenuHandler::class);
        $this->em = $kernel->getContainer()->get('doctrine')->getManager();
        $this->repo = parent::$container->get('BadgerCMS\Domain\Page\Repository\MenuRepositoryInterface');
    }

    public function testShouldEditMenuAndCheckAuditLog()
    {
        $newTitle = 'New title for test menu';
        /** @var Menu $testMenu */
        $testMenu = $this->repo->find(TestFixtures::MENU_UUID);
        $testMenu->setTitle($newTitle);
        $cmd = new EditMenuCommand($testMenu);
        $this->handler->__invoke($cmd);

        unset($testMenu);

        $testMenu = $this->repo->find(TestFixtures::MENU_UUID);
        $this->assertEquals($newTitle, $testMenu->getTitle());

        //TODO: check Audit log
    }
}
