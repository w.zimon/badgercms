<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Form\ContentType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class HomePageResourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('seo', SEOType::class,
                [
                    'label' => 'Ustawienia SEO',
                    'attr'  => [
                        'class' => 'collapse-fieldset'
                    ]
                ]
            )
            ->add('welcome', TextType::class,
                [
                    'label'    => 'zielone powitanie (h1)',
                    'required' => false,
                    'attr'     => [
                        'class' => 'form-control form-line form-control'
                    ]
                ])
            ->add('subWelcome', TextType::class,
                [
                    'label'    => 'pod zielonym nagłówkiem (h2)',
                    'required' => false,
                    'attr'     => [
                        'class' => 'form-control form-line form-control'
                    ]
                ])
            ->add('getStarted', LinkType::class,
                [
                    'label'    => 'getStarted',
                    'required' => false,
                    'attr'     => [
                        'class' => 'collapse-fieldset'
                    ]
                ])
            ->add('socialLinks', CollectionType::class, [
                'entry_type' => LinkType::class,
                'label'      => 'Linki do sklepów',
                'empty_data' => [ // predefined 3 items, without ability to add, remove items
                                  ['href' => '#'],
                                  ['href' => '#'],
                                  ['href' => '#']
                ],
                'attr'       => [
                    'class' => 'collapse-fieldset'
                ],
            ])
            ->add('claim', TextType::class,
                [
                    'label'    => 'claim',
                    'required' => false,
                    'attr'     => [
                        'class' => 'form-control form-line form-control'
                    ]
                ])
            ->add('resource', TextType::class,
                [
                    'label'    => 'test',
                    'required' => false,
                    'attr'     => [
                        'class' => 'form-control form-line form-control'
                    ]
                ]);

    }
}