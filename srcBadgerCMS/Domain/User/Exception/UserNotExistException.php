<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\User\Exception;

class UserNotExistException extends \Exception
{

}