<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\User;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Gedmo\Mapping\Annotation as Gedmo;

trait UserModel
{

    /**
     * @var UuidInterface
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private UuidInterface $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, unique=true)
     * @Gedmo\Versioned
     */
    private string $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, unique=true)
     * @Gedmo\Versioned
     */
    private string $login;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private ?string $password;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Gedmo\Versioned
     */
    private ?string $name = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Gedmo\Versioned
     */
    private ?string $surname = null;

    /**
     * @var array|string[]
     * @ORM\Column(type="array")
     * @Gedmo\Versioned
     */
    private array $role = ['ROLE_USER'];

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": true})
     * @Gedmo\Versioned
     */
    private bool $active = true;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": false})
     * @Gedmo\Versioned
     */
    private bool $blocked = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": false})
     * @Gedmo\Versioned
     */
    private bool $removed = false;

}