<?php

namespace BadgerCMS\Application\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminSidebarMenuBuilder
{
    private FactoryInterface $factory;
    private TranslatorInterface $translator;

    public function __construct(FactoryInterface $factory, TranslatorInterface $translator)
    {
        $this->factory = $factory;
        $this->translator = $translator;
    }

    public function createMainMenu(array $options): ItemInterface
    {

        $menu = $this->factory->createItem('root');

        $menu->addChild('Home1', ['uri' => '#', 'extras' => ['icon' => '<i class="ti-dashboard"></i>']]);
        $menu['Home1']->addChild('sub Home1', ['route' => 'admin_dashboard']);
        $menu['Home1']->addChild('sub Home2', ['route' => 'admin_dashboard']);
        $menu['Home1']->addChild('sub Home3', ['route' => 'admin_dashboard']);
        $menu->addChild('Home2', ['route' => 'admin_dashboard']);

        return $menu;
    }
}