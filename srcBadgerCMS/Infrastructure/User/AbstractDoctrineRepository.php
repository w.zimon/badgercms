<?php
declare(strict_types=1);

namespace BadgerCMS\Infrastructure\User;

use BadgerCMS\Infrastructure\AbstractDoctrineRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

abstract class AbstractDoctrineRepository extends ServiceEntityRepository implements AbstractDoctrineRepositoryInterface
{

    protected PaginatorInterface $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        $entityClass = $this->getEntityClassName();
        $this->paginator = $paginator;
        parent::__construct(
            $registry,
            $entityClass
        );
    }

    abstract protected function getEntityClassName(): string;

    public function save($entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    public function remove($entity): void
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    protected function createConnectionQueryBuilder(): QueryBuilder
    {
        return $this->_em->getConnection()->createQueryBuilder();
    }

    protected function getPaginatedResult($qb, int $perPage, int $page): PaginationInterface
    {
        return $this->paginator->paginate($qb, $page, $perPage);
    }

}