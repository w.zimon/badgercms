<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\User\EditUser;

use BadgerCMS\Application\Command\User\AbstractUserHandler;
use BadgerCMS\Domain\User\ValueObject\AuthVO;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EditUserHandler extends AbstractUserHandler implements MessageHandlerInterface
{

    public function __invoke(EditUserCommand $editUserCommand)
    {
        $user = $editUserCommand->getUser();
        if ($user->getPassword() != null) {
            $password = $this->passwordEncoder->encodePassword(new AuthVO(), $user->getPassword());
        } else {
            $password = $this->userRepository->getCurrentUserPassword($user->getId());
        }
        $user->setPassword($password);

        $this->userRepository->save($user);
    }
}