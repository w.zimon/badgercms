<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\User\DeleteUser;

use BadgerCMS\Application\Command\User\AbstractUserCommand;

class DeleteUserCommand extends AbstractUserCommand
{

}