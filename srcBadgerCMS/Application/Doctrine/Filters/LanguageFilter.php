<?php


namespace BadgerCMS\Application\Doctrine\Filters;


use BadgerCMS\Application\Common\LanguageDependentInterface;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Doctrine\Persistence\Mapping\ClassMetadata;

class LanguageFilter extends SQLFilter
{
    const LABEL = 'language';

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {

        if ($targetEntity->getReflectionClass()->implementsInterface(LanguageDependentInterface::class)) {
            return sprintf('%s.lang_id = %s', $targetTableAlias, $this->getParameter('lang'));
        }
        return '';
    }
}