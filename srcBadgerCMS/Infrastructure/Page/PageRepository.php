<?php
declare(strict_types=1);

namespace BadgerCMS\Infrastructure\Page;

use BadgerCMS\Domain\Page\Exception\PageWithIdNotExistException;
use BadgerCMS\Domain\Page\Page;
use BadgerCMS\Domain\Page\Repository\PageRepositoryInterface;
use BadgerCMS\Infrastructure\User\AbstractDoctrineRepository;


class PageRepository extends AbstractDoctrineRepository implements PageRepositoryInterface
{
    const TABLE = 'pages';

    protected function getEntityClassName(): string
    {
        return Page::class;
    }

    public function getAll(string $menuId = null): ?array
    {
        $qb = $this->createConnectionQueryBuilder();
        $qb->select('node.*')
            ->from(self::TABLE, 'node')
            ->from(self::TABLE, 'parent')
            ->where('node.lft BETWEEN parent.lft AND parent.rgt');

        if ($menuId === null) {
            $qb->where('node.menu_id IS NULL');
        } else {
            $qb->where('node.menu_id = :menuId')
                ->setParameter('menuId', $menuId);
        }

        $qb->orderBy('node.lvl', 'ASC')
            ->addOrderBy('node.lft')
            ->orderBy('node.sort', 'ASC')
            ->groupBy('node.id');

        //$this->setLanguageCondition($qb, 'node');
        $result = $qb->execute()->fetchAll();
        if ($result) {
            return $this->getTreeGenerator()->generate($result);
        } else {
            return null;
        }
    }

    public function getForAdminEdit(string $id): array
    {
        $qb = $this->createConnectionQueryBuilder();
        $qb->select('*')
            ->from(self::TABLE)
            ->where('id = :id')
            ->setParameter('id', $id);

        //$this->setLanguageCondition($qb);
        $data = $qb->execute()->fetch();
        if (!$data) {
            throw new PageWithIdNotExistException($id);
        }
        $data['role'] = $data['access_role'];

        if (isset($data['content']) && is_string($data['content'])) {
            $data['content'] = unserialize($data['content']);
        }

        return $data;
    }


    public function checkIfSlugExist(string $slug, string $id = null): bool
    {
        $qb = $this->createConnectionQueryBuilder();
        $qb->select('id')
            ->from(self::TABLE)
            ->where('slug = :slug')
            ->setParameter('slug', $slug);

        if ($id) {
            $qb->andWhere('id <> :id')
                ->setParameter('id', $id);
        }

        // $this->setLanguageCondition($qb);
        $data = $qb->execute()->fetch();

        if ($data) {
            return true;
        } else {
            return false;
        }
    }

    public function getOneBySlug(string $slug): ?Page
    {
        // frontend conditions!
        $qb = $this->createQueryBuilder('p');
        $qb->select('*')
            ->from(self::TABLE, 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug);

        // $this->setLanguageCondition($qb);  setAdmin or Front Conditions, admin - all, front only public, active, roles
        //$this->setLanguageCondition($qb);
        /** @var Page $page */
        $page = $qb->getFirstResult();
        if (is_object($page)) {
            return $page;
        } else {
            return null;
        }
    }

    public function delete(string $id): void
    {
        $qb = $this->createConnectionQueryBuilder();
        $qb->delete()
            ->from(self::TABLE)
            ->where('id = :id')
            ->setParameter('id', $id);

        //  $this->setLanguageCondition($qb);
        $qb->execute();
    }

}
