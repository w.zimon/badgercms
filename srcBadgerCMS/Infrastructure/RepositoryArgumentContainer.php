<?php

namespace BadgerCMS\Infrastructure;



use BadgerCMS\Application\Services\LanguageService;
use BadgerCMS\Infrastructure\User\AbstractDoctrineRepository;

abstract class RepositoryArgumentContainer extends AbstractDoctrineRepository
{
    private $languageService;

    public function __construct(LanguageService $languageService)
    {
        $this->languageService = $languageService;
    }

    public function getLanguageService(): LanguageService
    {
        return $this->languageService;
    }

}