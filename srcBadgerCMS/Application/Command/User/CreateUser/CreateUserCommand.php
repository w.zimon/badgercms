<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\User\CreateUser;

use BadgerCMS\Application\Command\User\AbstractUserCommand;

class CreateUserCommand extends AbstractUserCommand
{

}