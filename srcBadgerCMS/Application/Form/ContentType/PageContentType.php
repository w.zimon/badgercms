<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Form\ContentType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PageContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('seo', SEOType::class,
                [
                    'label' => SEOType::LABEL,
                    'attr'  => [
                        'class' => 'collapse-fieldset'
                    ]
                ]
            )
            ->add('ogMeta', OgMetaType::class,
                [
                    'label' => OgMetaType::LABEL,
                    'attr'  => [
                        'class' => 'collapse-fieldset'
                    ]
                ]
            )
            ->add('body', TextType::class,
                [
                    'label'    => 'body',
                    'required' => false,
                    'attr'     => [
                        'class' => 'form-control form-line form-control'
                    ]
                ]);
        $builder->addModelTransformer(new CallbackTransformer(
            function ($tagsAsArray) {
                dump($tagsAsArray);
                return $tagsAsArray;
            },
            function ($tagsAsString) {
                dump($tagsAsString);
                return $tagsAsString;
            }
        ));

    }
}