<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\User\Exception;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

/**
 * Class AccountInactiveException
 *
 * @author  : Wojciech Zimoń <w.zimon@inis.pl>
 * @package CMS\Domain\User\Exception
 */
class AccountInactiveException extends AccountStatusException
{

    public function getMessageKey(): string
    {
        return 'This account is inactive.';
    }
}
