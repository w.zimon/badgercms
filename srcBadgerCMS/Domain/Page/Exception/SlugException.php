<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Page\Exception;

use Exception;
use Throwable;

/**
 *
 * @author Wojciech Zimoń <sebastian.procek@gmail.com>
 * @package CMS\Domain\Page\Exception\SlugException
 **/
class SlugException extends Exception
{
    const MSG_ONLY_ONE_EMPTY_SLUG = 'Only one page in specific language, should has empty slug(for homepage)';

}
