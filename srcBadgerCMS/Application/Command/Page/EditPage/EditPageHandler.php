<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\Page\EditPage;

use BadgerCMS\Application\Command\Page\AbstractPageHandler;
use BadgerCMS\Domain\Page\Page;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EditPageHandler extends AbstractPageHandler implements MessageHandlerInterface
{

    /**
     * @param EditPageCommand $cmd
     */
    public function __invoke(EditPageCommand $cmd): void
    {
        $page = $cmd->getPage();
        $page->setContent($page->getContent());
        $this->pageRepository->save($page);
    }
}