<?php

namespace BadgerCMS\Domain\Page;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

trait PageModel
{

    /**
     * @param UuidInterface $id
     * @return Page
     */
    public static function create(UuidInterface $id): Page
    {
        $page = new Page();
        $page->id = $id;

        return $page;
    }

    /**
     * @var UuidInterface
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private UuidInterface $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=512)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=Menu::class, mappedBy="page", cascade={"persist", "remove"})
     */

    private $menus;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $accessRole;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $active = true;


    /**
     * @var string
     * @ORM\Column(type="text", options={"default": "{}"}, nullable=true)
     */
    protected $content;

    /**
     * @var string
     * @ORM\Column(type="string", length=512)
     */
    protected $type;


}