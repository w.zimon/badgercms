<?php
declare(strict_types=1);

namespace BadgerCMS\UI\Controller;


class DefaultSiteController extends AbstractAdminController
{
    public function index()
    {
        return $this->render('default_site/index.html.twig', [
            'controller_name' => 'SiteController',
        ]);
    }
}
