<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\User;

use BadgerCMS\Domain\Common\LifecycleTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 * @package BadgerCMS\Domain\User
 * @ORM\Entity(repositoryClass="BadgerCMS\Infrastructure\User\UserRepository")
 * @ORM\Table(name="users", indexes={@ORM\Index(name="email_idx", columns={"email"})})
 * @Gedmo\Loggable
 */
class User implements UserInterface
{

    use UserModel;
    use LifecycleTrait;

    public static function create(
        UuidInterface $id,
        string $login,
        string $email,
        array $role,
        string $password
    ) {
        $user = new self();
        $user->setId($id);
        $user->setLogin($login);
        $user->setEmail($email);
        $user->setRole($role);
        $user->setPassword($password);

        return $user;
    }

    public function __toString(): string
    {
        return $this->login . "({$this->email})";
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @param UuidInterface $id
     */
    public function setId(UuidInterface $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     */
    public function setSurname(?string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return array|string[]
     */
    public function getRoles()
    {
        return $this->role;
    }

    /**
     * @return array|string[]
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param array|string[] $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    /**
     * @param bool $blocked
     */
    public function setBlocked(bool $blocked): void
    {
        $this->blocked = $blocked;
    }

    /**
     * @return bool
     */
    public function isRemoved(): bool
    {
        return $this->removed;
    }

    /**
     * @param bool $removed
     */
    public function setRemoved(bool $removed): void
    {
        $this->removed = $removed;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        return;
    }


}