<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Page\Repository;

use Doctrine\Common\Collections\Criteria;

class RepositoryCriteria
{

    public static function adminMainRootsListCriteria(): Criteria
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('lvl', 0));

        return $criteria;
    }

}