#!/bin/bash

Env=$(grep APP_ENV .env | cut -d '=' -f2)
echo -n "[Run it only on dev] Are You sure? This script remove your database (y/n)? "
read answer

if [ "$answer" != "${answer#[Yy]}" ] && [ "$Env" != "prod" ] ;then
    cp public/local.htaccess public/.htaccess
    composer install --no-scripts
    yarn

    ####setup database create table and load fixtures
    ./bin/console doctrine:database:drop --force --if-exists
    ./bin/console doctrine:database:create
    ./bin/console doctrine:migrations:migrate --no-interaction
    ./bin/console doctrine:fixtures:load --group=setup --no-interaction

    ###create user
    ./bin/console badger:user:create test@test.pl admin admin 1

    ./bin/console fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json
    ####run tests
    echo y | sh ./etc/setup_test.sh
    ./bin/phpunit
else
    echo exit
fi