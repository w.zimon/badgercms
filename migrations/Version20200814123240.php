<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200814123240 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE langs (id VARCHAR(2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menus (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', page_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', lang_id VARCHAR(2) DEFAULT NULL, tree_root CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', parent_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', placeholder VARCHAR(255) NOT NULL, remote_url VARCHAR(512) DEFAULT NULL, remote_url_target VARCHAR(255) DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, active TINYINT(1) NOT NULL, lft INT NOT NULL, rgt INT NOT NULL, lvl INT NOT NULL, sort INT NOT NULL, INDEX IDX_727508CFC4663E4 (page_id), INDEX IDX_727508CFB213FA4 (lang_id), INDEX IDX_727508CFA977936C (tree_root), INDEX IDX_727508CF727ACA70 (parent_id), INDEX active_idx (active), INDEX sort_idx (sort), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pages (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, access_role VARCHAR(255) DEFAULT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, content VARCHAR(64) NOT NULL, type VARCHAR(64) NOT NULL, UNIQUE INDEX UNIQ_2074E575989D9B62 (slug), UNIQUE INDEX UNIQ_2074E575FEC530A9 (content), UNIQUE INDEX UNIQ_2074E5758CDE5729 (type), INDEX slug_idx (slug), INDEX active_idx (active), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE menus ADD CONSTRAINT FK_727508CFC4663E4 FOREIGN KEY (page_id) REFERENCES pages (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE menus ADD CONSTRAINT FK_727508CFB213FA4 FOREIGN KEY (lang_id) REFERENCES langs (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE menus ADD CONSTRAINT FK_727508CFA977936C FOREIGN KEY (tree_root) REFERENCES menus (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menus ADD CONSTRAINT FK_727508CF727ACA70 FOREIGN KEY (parent_id) REFERENCES menus (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE menus DROP FOREIGN KEY FK_727508CFB213FA4');
        $this->addSql('ALTER TABLE menus DROP FOREIGN KEY FK_727508CFA977936C');
        $this->addSql('ALTER TABLE menus DROP FOREIGN KEY FK_727508CF727ACA70');
        $this->addSql('ALTER TABLE menus DROP FOREIGN KEY FK_727508CFC4663E4');
        $this->addSql('DROP TABLE langs');
        $this->addSql('DROP TABLE menus');
        $this->addSql('DROP TABLE pages');
    }
}
