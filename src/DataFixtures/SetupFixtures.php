<?php

namespace App\DataFixtures;

use BadgerCMS\Domain\Language\Language;
use BadgerCMS\Domain\Page\Menu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class SetupFixtures extends Fixture implements FixtureGroupInterface
{
    const PL = 'pl';
    const GROUP_FOR_SETUP_SH = 'setup';

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $rootMenu = Menu::create(Uuid::uuid4(), 'MAIN_MENU', 'main menu', 'from fixtures');
        $this->save(
            new Language(self::PL),
            $rootMenu,
            Menu::create(Uuid::uuid4(), 'SUB_MAIN_MENU', 'sub menu 1' , 'from fixtures'),
            Menu::create(Uuid::uuid4(), 'SUB_MAIN_MENU', 'main menu 2', 'from fixtures'),
            Menu::create(Uuid::uuid4(), 'SUB_MAIN_MENU', 'main menu 3', 'from fixtures')
        );
    }

    private function save(...$entities)
    {
        foreach ($entities as $entity) {
            $this->manager->persist($entity);
        }

        $this->manager->flush();
    }
    public static function getGroups(): array
    {
        return [self::GROUP_FOR_SETUP_SH];
    }
}
