<?php

namespace App\DataFixtures;

use BadgerCMS\Domain\Language\Language;
use BadgerCMS\Domain\Page\Menu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class TestFixtures extends Fixture implements FixtureGroupInterface
{
    const PL = 'pl';
    const GROUP_FOR_SETUP_TEST_SH = 'test';
    const MENU_PLACEHOLDER = '_TEST_MENU_';
    const MENU_UUID = '5496df4d-3504-456c-97cb-c5bad0c31bfc';
    /**
     * @var ObjectManager
     */
    private ObjectManager $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->save(
            new Language(self::PL),
            Menu::create(Uuid::fromString(self::MENU_UUID), self::MENU_PLACEHOLDER, 'title', 'from  test fixtures')
        );
    }

    private function save(...$entities)
    {
        foreach ($entities as $entity) {
            $this->manager->persist($entity);
        }

        $this->manager->flush();
    }

    public static function getGroups(): array
    {
        return [
            self::GROUP_FOR_SETUP_TEST_SH
        ];
    }
}
