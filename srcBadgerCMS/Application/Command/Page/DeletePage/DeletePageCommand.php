<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\Page\DeletePage;

use BadgerCMS\Application\Command\Page\AbstractPageCommand;

class DeletePageCommand extends AbstractPageCommand
{

}