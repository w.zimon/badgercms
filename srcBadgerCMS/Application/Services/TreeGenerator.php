<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Services;

class TreeGenerator
{
    /**
     * @param array $list
     * @return array
     */
    public function generate(array $list): array
    {
        $parents = [];
        foreach ($list as $item) {
            $parentId = $item['parent_id'] ?? 0;
            $parents[$parentId][] = $item;
        }
        return $this->createBranch($parents, $parents[0]);
    }

    /**
     * @param $parents
     * @param $children
     * @return array
     */
    private function createBranch(&$parents, $children): array
    {
        $tree = [];
        foreach ($children as $child) {
            if (isset($parents[$child['id']])) {
                $child['children'] = $this->createBranch($parents, $parents[$child['id']]);
            }
            $tree[] = $child;
        }
        return $tree;
    }

}
