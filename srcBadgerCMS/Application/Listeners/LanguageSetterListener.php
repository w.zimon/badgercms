<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Listeners;

use BadgerCMS\Application\Common\LanguageDependentInterface;
use BadgerCMS\Domain\Language\Language;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class LanguageSetterListener
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var string
     */
    private $lang;
    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBag;
    /**
     * @var RequestStack
     */
    private RequestStack $requestStack;

    /**
     * ContextListener constructor.
     * @param EntityManagerInterface $em
     * @param RequestStack $requestStack
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack,
        ParameterBagInterface $parameterBag
    ) {
        $this->em = $em;
        $masterRequest = $requestStack->getMasterRequest();
        $this->lang = is_object($masterRequest) ? $masterRequest->getLocale() : $parameterBag->get('default_locale');
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if ($entity instanceof LanguageDependentInterface) {
            $entity->setLang($args->getObjectManager()->getReference(Language::class, $this->lang));
        }
    }
}