<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\User\Exception;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

/**
 * Class AccountDeletedException
 *
 * @author  : Wojciech Zimoń <w.zimon@inis.pl>
 * @package CMS\Domain\User\Exception
 */
class AccountBlockedException extends AccountStatusException
{

    public function getMessageKey(): string
    {
        return 'This account is blocked.';
    }
}
