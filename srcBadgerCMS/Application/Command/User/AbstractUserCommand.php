<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\User;

use BadgerCMS\Domain\User\User;

abstract class AbstractUserCommand
{

    protected User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }

}