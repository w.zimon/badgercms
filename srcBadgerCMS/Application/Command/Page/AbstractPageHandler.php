<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\Page;

use BadgerCMS\Domain\Page\Repository\PageRepositoryInterface;

abstract class AbstractPageHandler
{

    /**
     * @var PageRepositoryInterface
     */
    protected PageRepositoryInterface $pageRepository;

    public function __construct(PageRepositoryInterface $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

}