<?php


namespace BadgerCMS\Application\Doctrine\Filters;


use BadgerCMS\Domain\Page\Menu;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Doctrine\Persistence\Mapping\ClassMetadata;

class CrudMenuFilter extends SQLFilter
{
    const LABEL = 'crud_menu';

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if ($targetEntity->getName() === Menu::class) {
            $parent_id = $this->getParameter('parent_id');
            if ($parent_id == "''") {
                return sprintf('%s.parent_id IS NULL', $targetTableAlias);
            } else {
                return sprintf('%s.parent_id = %s', $targetTableAlias, $parent_id);
            }

        }
        return '';
    }
}