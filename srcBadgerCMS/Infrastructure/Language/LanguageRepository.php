<?php
declare(strict_types=1);

namespace BadgerCMS\Infrastructure\Language;

use BadgerCMS\Domain\Language\Language;
use BadgerCMS\Infrastructure\User\AbstractDoctrineRepository;


class LanguageRepository extends AbstractDoctrineRepository
{
    const TABLE = 'langs';

    protected function getEntityClassName(): string
    {
        return Language::class;
    }

}