<?php
declare(strict_types=1);

namespace BadgerCMS\UI\Controller;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ChangeLocaleController extends AbstractAdminController
{
    /**
     * @Route("/locale/{_locale}", name="locale")
     */
    public function index(Request $request)
    {
        $referer = $request->headers->get('referer');

        return new RedirectResponse($referer);
    }
}
