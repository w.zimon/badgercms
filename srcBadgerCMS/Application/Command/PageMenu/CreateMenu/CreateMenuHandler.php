<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\PageMenu\CreateMenu;

use BadgerCMS\Application\Command\PageMenu\AbstractMenuHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;


class CreateMenuHandler extends AbstractMenuHandler implements MessageHandlerInterface
{

    public function __invoke(CreateMenuCommand $cmd): void
    {
        $this->menuRepository->save($cmd->getMenu());
    }
}