<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\Page\Repository;


use BadgerCMS\Domain\Page\Menu;
use BadgerCMS\Infrastructure\AbstractDoctrineRepositoryInterface;
use Doctrine\Common\Collections\Criteria;
use Knp\Component\Pager\Pagination\PaginationInterface;


interface MenuRepositoryInterface extends AbstractDoctrineRepositoryInterface
{
    public function getList(int $page = 1, int $perPage = 25, Criteria $criteria = null): PaginationInterface;

    public function getForAdminEdit(string $id): array;

    public function findById(string $id): Menu;

    public function getOneByPlaceholder(string $title): ?Menu;

}