<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\PageMenu\DeleteMenu;


use BadgerCMS\Application\Command\PageMenu\AbstractMenuCommand;

class DeleteMenuCommand extends AbstractMenuCommand
{

}