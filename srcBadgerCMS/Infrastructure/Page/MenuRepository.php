<?php
declare(strict_types=1);

namespace BadgerCMS\Infrastructure\Page;

use BadgerCMS\Domain\Page\Exception\MenuWithIdNotExistException;
use BadgerCMS\Domain\Page\Menu;
use BadgerCMS\Domain\Page\Repository\MenuRepositoryInterface;
use BadgerCMS\Infrastructure\User\AbstractDoctrineRepository;
use Doctrine\Common\Collections\Criteria;
use Knp\Component\Pager\Pagination\PaginationInterface;


class MenuRepository extends AbstractDoctrineRepository implements MenuRepositoryInterface
{
    const TABLE = 'menus';
    const ALIAS = 'm';

    protected function getEntityClassName(): string
    {
        return Menu::class;
    }

    public function getList(int $page = 1, int $perPage = 25, Criteria $criteria = null): PaginationInterface
    {
        $qb = $this->createQueryBuilder(self::ALIAS);
        if ($criteria instanceof Criteria) {
            $qb->addCriteria($criteria);
        }

        return $this->getPaginatedResult($qb->getQuery(), $perPage, $page);
    }

    public function getForAdminEdit(string $id): array
    {
        $qb = $this->createConnectionQueryBuilder();
        $qb->select('*')
            ->from(self::TABLE)
            ->where('id = :id')
            ->setParameter('id', $id);

        // $this->setLanguageCondition($qb);
        $data = $qb->execute()->fetch();
        if (!$data) {
            throw new MenuWithIdNotExistException($id);
        }

        return $data;
    }

    public function findById(string $id): Menu
    {
        $qb = $this->createQueryBuilder('m');
        $qb->select('*')
            ->from(self::TABLE, 'm')
            ->where('id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1);

        //  $this->setLanguageCondition($qb);
        $menu = $qb->getFirstResult();

        if (is_object($menu)) {
            return $menu;
        } else {
            throw new MenuWithIdNotExistException($id);
        }
    }


    public function getOneByPlaceholder(string $title): ?Menu
    {
        // TODO: Implement getOneByPlaceholder() method.
    }
}