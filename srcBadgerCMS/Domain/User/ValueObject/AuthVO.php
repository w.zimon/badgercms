<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\User\ValueObject;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Serializable;

class AuthVO implements UserInterface, Serializable
{

    protected UuidInterface $id;
    protected string $email;
    protected string $login;
    protected array $roles;
    protected string $password;
    protected bool $active;
    protected bool $removed;
    protected bool $blocked;

    public function __construct(array $data = [])
    {
        if (!empty($data)) {
            $this->id = Uuid::fromString($data['id']);
            $this->email = $data['email'];
            $this->login = $data['login'];
            $this->roles = unserialize($data['role']);
            $this->password = $data['password'];
            $this->active = (bool)$data['active'];
            $this->removed = (bool)$data['removed'];
            $this->blocked = (bool)$data['blocked'];
        }
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->login;
    }

    public function eraseCredentials()
    {
        return;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    public function isRemoved(): bool
    {
        return $this->removed;
    }

    public function serialize()
    {
        return serialize(
            [
                $this->login,
                $this->password,
            ]
        );
    }

    public function unserialize($serialized)
    {
        list (
            $this->login,
            $this->password,
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }

}