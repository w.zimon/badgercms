<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\Page\CreatePage;


use BadgerCMS\Application\Command\Page\AbstractPageHandler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreatePageHandler extends AbstractPageHandler implements MessageHandlerInterface
{

    /**
     * @param CreatePageCommand $cmd
     */
    public function __invoke(CreatePageCommand $cmd): void
    {
        dump($cmd->getPage());
        $this->pageRepository->save($cmd->getPage());
    }
}