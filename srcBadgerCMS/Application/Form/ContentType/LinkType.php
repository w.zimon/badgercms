<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Form\ContentType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LinkType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('href', TextType::class, [
                'required' => false,
            ])
            ->add('label', TextType::class, [
                'required' => false,
            ])
            ->add('rel', ChoiceType::class, [
                'required' => false,
                'choices'  => [
                    'alternate'  => 'alternate',
                    'author'     => 'author',
                    'bookmark'   => 'bookmark',
                    'external'   => 'external',
                    'help'       => 'help',
                    'license'    => 'license',
                    'next'       => 'next',
                    'nofollow'   => 'nofollow',
                    'noreferrer' => 'noreferrer',
                    'noopener'   => 'noopener',
                    'prev'       => 'prev',
                    'search'     => 'search',
                    'tag'        => 'tag'
                ]
            ])
            ->add('target', ChoiceType::class, [
                'required' => false,
                'choices'  => ['_blank' => '_blank'],
                'label'    => 'target',
                'help' => '_blank open in new window'
            ]);


    }
    public function configureOptions(OptionsResolver $resolver)
    {

    }

}