<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200917192008 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_2074E575FEC530A9 ON pages');
        $this->addSql('DROP INDEX UNIQ_2074E5758CDE5729 ON pages');
        $this->addSql('ALTER TABLE pages CHANGE title title VARCHAR(512) NOT NULL, CHANGE access_role access_role VARCHAR(512) DEFAULT NULL, CHANGE content content LONGTEXT NOT NULL, CHANGE type type VARCHAR(512) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pages CHANGE title title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE access_role access_role VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE content content VARCHAR(64) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE type type VARCHAR(64) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2074E575FEC530A9 ON pages (content)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2074E5758CDE5729 ON pages (type)');
    }
}
