<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200813184233 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE users (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', email VARCHAR(64) NOT NULL, login VARCHAR(64) NOT NULL, password LONGTEXT NOT NULL, name VARCHAR(64) DEFAULT NULL, surname VARCHAR(64) DEFAULT NULL, role LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', active TINYINT(1) DEFAULT \'1\' NOT NULL, blocked TINYINT(1) DEFAULT \'0\' NOT NULL, removed TINYINT(1) DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), UNIQUE INDEX UNIQ_1483A5E9AA08CB10 (login), INDEX email_idx (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE users');
    }
}
