<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\User\EditUser;

use BadgerCMS\Application\Command\User\AbstractUserCommand;

class EditUserCommand extends AbstractUserCommand
{

}