<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200904203029 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE menus ADD page_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE menus ADD CONSTRAINT FK_727508CFC4663E4 FOREIGN KEY (page_id) REFERENCES pages (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_727508CFC4663E4 ON menus (page_id)');
        $this->addSql('ALTER TABLE pages DROP FOREIGN KEY FK_2074E57514041B84');
        $this->addSql('DROP INDEX IDX_2074E57514041B84 ON pages');
        $this->addSql('ALTER TABLE pages DROP menus_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE menus DROP FOREIGN KEY FK_727508CFC4663E4');
        $this->addSql('DROP INDEX IDX_727508CFC4663E4 ON menus');
        $this->addSql('ALTER TABLE menus DROP page_id');
        $this->addSql('ALTER TABLE pages ADD menus_id CHAR(36) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE pages ADD CONSTRAINT FK_2074E57514041B84 FOREIGN KEY (menus_id) REFERENCES menus (id)');
        $this->addSql('CREATE INDEX IDX_2074E57514041B84 ON pages (menus_id)');
    }
}
