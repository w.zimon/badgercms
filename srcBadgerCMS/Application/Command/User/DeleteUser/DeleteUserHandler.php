<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\User\DeleteUser;

use BadgerCMS\Application\Command\User\AbstractUserHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DeleteUserHandler extends AbstractUserHandler implements MessageHandlerInterface
{

    public function __invoke(DeleteUserCommand $deleteUserCommand)
    {
        $user = $deleteUserCommand->getUser();
        $this->userRepository->remove($user);
    }

}