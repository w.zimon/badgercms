<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200904185022 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pages ADD lang_id VARCHAR(2) DEFAULT NULL');
        $this->addSql('ALTER TABLE pages ADD CONSTRAINT FK_2074E575B213FA4 FOREIGN KEY (lang_id) REFERENCES langs (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_2074E575B213FA4 ON pages (lang_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pages DROP FOREIGN KEY FK_2074E575B213FA4');
        $this->addSql('DROP INDEX IDX_2074E575B213FA4 ON pages');
        $this->addSql('ALTER TABLE pages DROP lang_id');
    }
}
