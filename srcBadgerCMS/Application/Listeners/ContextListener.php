<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Listeners;

use BadgerCMS\Application\Doctrine\Filters\LanguageFilter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ContextListener
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * ContextListener constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $this->em->getFilters()->enable(LanguageFilter::LABEL)->setParameter('lang', $event->getRequest()->getLocale());
    }
}