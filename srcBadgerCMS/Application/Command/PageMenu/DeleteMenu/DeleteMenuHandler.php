<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\PageMenu\DeleteMenu;

use BadgerCMS\Application\Command\PageMenu\AbstractMenuHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;


class DeleteMenuHandler extends AbstractMenuHandler implements MessageHandlerInterface
{

    public function __invoke(DeleteMenuCommand $command): void
    {
        $this->menuRepository->remove($command->getMenu());
    }
}