<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\PageMenu;

use BadgerCMS\Domain\Page\Menu;

abstract class AbstractMenuCommand
{

    protected Menu $menu;

    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }

    public function getMenu(): Menu
    {
        return $this->menu;
    }

}