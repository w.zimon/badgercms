<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\User\CreateUser;

use BadgerCMS\Application\Command\User\AbstractUserHandler;
use BadgerCMS\Domain\User\ValueObject\AuthVO;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateUserHandler extends AbstractUserHandler implements MessageHandlerInterface
{

    public function __invoke(CreateUserCommand $createUserCommand)
    {
        $user = $createUserCommand->getUser();
        $password = $this->passwordEncoder->encodePassword(new AuthVO(), $user->getPassword());
        $user->setPassword($password);

        $this->userRepository->save($user);
    }

}