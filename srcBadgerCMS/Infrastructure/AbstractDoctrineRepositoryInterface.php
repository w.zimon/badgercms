<?php

namespace BadgerCMS\Infrastructure;


interface AbstractDoctrineRepositoryInterface
{
    function save($entity): void;

    function remove($entity): void;
}