<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Form\ContentType;

use CMS\Modules\Media\Application\Form\ContentType\MediaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;

class OgMetaType extends AbstractType
{
    const LABEL = 'Ustawienia Open Graph (facebook)';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', TextType::class, [
                'required' => false
            ])
            ->add('description', TextType::class, [
                'required' => false
            ]);


    }


}