<?php

namespace BadgerCMS\UI\Controller\Admin;

use BadgerCMS\Application\Command\PageMenu\CreateMenu\CreateMenuCommand;
use BadgerCMS\Application\Command\PageMenu\DeleteMenu\DeleteMenuCommand;
use BadgerCMS\Application\Command\PageMenu\EditMenu\EditMenuCommand;
use BadgerCMS\Application\Doctrine\Filters\CrudMenuFilter;
use BadgerCMS\Application\Fields\MenuChildrenField;
use BadgerCMS\Domain\Page\Menu;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Ramsey\Uuid\Uuid;

class MenuCrudController extends AbstractCrudController
{
    /**
     * @var CrudUrlGenerator
     */
    private CrudUrlGenerator $crudUrlGenerator;

    public function __construct(CrudUrlGenerator $crudUrlGenerator)
    {
        $this->crudUrlGenerator = $crudUrlGenerator;
    }

    public function createEntity(string $entityFqcn)
    {
        $parent = (isset($_GET['parent_id'])) ? $this->getDoctrine()->getManager()->getReference(Menu::class,
            Uuid::fromString($_GET['parent_id'])) : null;
        return Menu::create(Uuid::uuid4(), '', '', '', $parent);
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $cmd = new CreateMenuCommand($entityInstance);
        $this->dispatchMessage($cmd);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $cmd = new EditMenuCommand($entityInstance);
        $this->dispatchMessage($cmd);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $cmd = new DeleteMenuCommand($entityInstance);
        $this->dispatchMessage($cmd);
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud->overrideTemplates([
            'crud/new'  => 'admin_crud/menu/new.html.twig',
            'crud/edit' => 'admin_crud/menu/edit.html.twig'
        ]);
        return parent::configureCrud($crud);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('title')
            ->add('description')
            ->add('active');
    }

    public function index(AdminContext $context)
    {
        $this->getDoctrine()
            ->getManager()
            ->getFilters()
            ->enable(CrudMenuFilter::LABEL)
            ->setParameter('parent_id', null);

        return parent::index($context);
    }

    public function new(AdminContext $context)
    {
        return parent::new($context);
    }

    public function configureResponseParameters(KeyValueStore $responseParameters): KeyValueStore
    {
        if (Crud::PAGE_NEW === $responseParameters->get('pageName')) {
            if (isset($_GET['parent_id'])) {
                $parent = $this->getDoctrine()
                    ->getManager()
                    ->getRepository(Menu::class)
                    ->findOneBy(['id' => $_GET['parent_id']]);
                $responseParameters->set('parent', $parent);
            }
        }

        return $responseParameters;
    }

    public function edit(AdminContext $context)
    {
        return parent::edit($context); // TODO: Change the autogenerated stub
    }

    public function configureActions(Actions $actions): Actions
    {
        $entityid = isset($_GET['entityId']) ? $_GET['entityId'] : null;
        $addSubMenu = Action::new('ADD_SUB_MENU', 'Dodaj pozycję menu', 'fa fa-edit')
            ->linkToUrl(
                $this->crudUrlGenerator
                    ->build()
                    ->setController(MenuCrudController::class)
                    ->setAction(Action::NEW)
                    ->set('parent_id', $entityid)
            );


        $actions->update(
            Crud::PAGE_INDEX,
            Action::EDIT,
            fn(Action $action) => $action->setIcon('fa fa-edit')->setLabel(false)
        )
            ->update(
                Crud::PAGE_INDEX,
                Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash')->setLabel(false)
            )
            ->update(
                Crud::PAGE_INDEX,
                Action::NEW,
                fn(Action $action) => $action->setLabel('Dodaj Menu')
            )
            ->add(
                Crud::PAGE_INDEX,
                Action::DETAIL
            )
            ->update(
                Crud::PAGE_INDEX,
                Action::DETAIL,
                fn(Action $action) => $action->setIcon('fa fa-eye')->setLabel(false)
            )
            ->add(Crud::PAGE_EDIT, $addSubMenu);

        return parent::configureActions($actions);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('title'),
            TextEditorField::new('description')->hideOnIndex(),
            AssociationField::new('page'),
            //      ->setCrudController(PageCrudController::class),
            BooleanField::new('active'),
            AssociationField::new('parent')->addCssClass('invisible'),
            MenuChildrenField::new('children')->onlyWhenUpdating(),
        ];
    }


    public static function getEntityFqcn(): string
    {
        return Menu::class;
    }
}
