<?php
declare(strict_types=1);

namespace BadgerCMS\UI\Command;

use BadgerCMS\Application\Command\User\CreateUser\CreateUserCommand as RegisterUserCommand;
use BadgerCMS\Domain\User\User;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Messenger\MessageBusInterface;


class CreateUserCommand extends Command
{

    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('badger:user:create')
            ->setDescription('Tworzenie nowego użytkownika.')
            ->setDefinition(
                array(
                    new InputArgument('email', InputArgument::REQUIRED, 'Email'),
                    new InputArgument('login', InputArgument::REQUIRED, 'Login'),
                    new InputArgument('password', InputArgument::REQUIRED, 'Hasło'),
                    new InputArgument('super-admin', InputArgument::REQUIRED, 'Uprawnienia'),
                )
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');
        $login = $input->getArgument('login');
        $password = $input->getArgument('password');
        $superAdmin = $input->getArgument('super-admin');

        $id = Uuid::uuid4();
        $role = (bool)$superAdmin ? ['ROLE_ADMIN'] : ['ROLE_USER'];
        $user = User::create($id, $login, $email, $role, $password);
        $command = new RegisterUserCommand($user);
        $this->messageBus->dispatch($command);

        $output->writeln(sprintf('Uzytkownik utworzony <comment>%s</comment>', $email));
        return 0;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();

        if (!$input->getArgument('email')) {
            $question = new Question('Prosze wybrac email:');
            $question->setValidator(
                function ($email) {
                    if (empty($email)) {
                        throw new Exception('Email nie moze byc pusty');
                    }

                    return $email;
                }
            );
            $questions['email'] = $question;
        }

        if (!$input->getArgument('login')) {
            $question = new Question('Prosze wybrac login:');
            $question->setValidator(
                function ($login) {
                    if (empty($login)) {
                        throw new Exception('Login nie moze byc pusty');
                    }

                    return $login;
                }
            );
            $questions['login'] = $question;
        }

        if (!$input->getArgument('password')) {
            $question = new Question('Prosze wybrac haslo:');
            $question->setValidator(
                function ($password) {
                    if (empty($password)) {
                        throw new Exception('Password can not be empty');
                    }

                    return $password;
                }
            );
            $question->setHidden(true);
            $questions['password'] = $question;
        }

        if (!$input->getArgument('super-admin')) {
            $question = new ChoiceQuestion(
                'Czy uzytkownik ma mieć prawa administracyjne?:',
                ['Nie', 'Tak'],
                0
            );
            $question->setValidator(
                function ($superAdmin) {
                    if ($superAdmin !== '0' && empty($superAdmin)) {
                        throw new Exception('Super-admin can not be empty');
                    }
                    if (strtolower($superAdmin) === 'nie') {
                        $superAdmin = 0;
                    }
                    return $superAdmin;
                }
            );
            $questions['super-admin'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
