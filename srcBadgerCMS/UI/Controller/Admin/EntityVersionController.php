<?php
declare(strict_types=1);

namespace BadgerCMS\UI\Controller\Admin;

use BadgerCMS\UI\Controller\AbstractAdminController;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Loggable\Entity\LogEntry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EntityVersionController
 * @package BadgerCMS\UI\Controller\Admin
 *
 * @Route("/entity-version")
 */
class EntityVersionController extends AbstractAdminController
{

    const ACTION_NAME = 'showVersionList';

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $entityClassName
     * @param string $entityId
     * @return Response
     *
     * @Route("/{entityClassName}/{entityId}", name="admin_entity_version")
     */
    public function index(string $entityClassName, string $entityId): Response
    {
        $loggableRepository = $this->entityManager->getRepository(LogEntry::class);
        $entityRepository = $this->entityManager->getRepository($entityClassName);

        $entity = $entityRepository->find($entityId);
        $logs = $loggableRepository->getLogEntries($entity);

        return $this->render(
            'admin_entity_version/index.html.twig',
            [
                'logs' => $logs
            ]
        );
    }

    /**
     * @param int $id
     * @param string $entityClassName
     * @param string $entityId
     * @return Response
     *
     * @Route("/{entityClassName}/{entityId}/{id}", name="admin_entity_detail", requirements={"id"="\d+"})
     */
    public function detail(string $entityClassName, string $entityId, int $id): Response
    {
        $loggableRepository = $this->entityManager->getRepository(LogEntry::class);
        $entityRepository = $this->entityManager->getRepository($entityClassName);

        $entity = $entityRepository->find($entityId);
        $logs = $loggableRepository->getLogEntries($entity);
        $log = $this->getLogEntry($id, $logs);

        return $this->render(
            'admin_entity_version/detail.html.twig',
            [
                'log' => $log,
                'isCurrentVersion' => $log->getVersion() === count($logs)
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $entityClassName
     * @param string $entityId
     * @param int $id
     * @return Response
     *
     * @Route("/revert/{entityClassName}/{entityId}/{id}", name="admin_entity_revert", requirements={"id"="\d+"})
     */
    public function revert(Request $request, string $entityClassName, string $entityId, int $id): Response
    {
        $loggableRepository = $this->entityManager->getRepository(LogEntry::class);
        $entityRepository = $this->entityManager->getRepository($entityClassName);

        $entity = $entityRepository->find($entityId);
        $logs = $loggableRepository->getLogEntries($entity);
        $log = $this->getLogEntry($id, $logs);

        $loggableRepository->revert($entity, $log->getVersion());
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->addFlash('success', sprintf('Encja została przywrócona do wersji %d', $log->getVersion()));
        return $this->redirectToRoute(
            'admin_entity_version',
            [
                'entityClassName' => $entityClassName,
                'entityId' => $entityId,
                'eaContext' => $request->get('eaContext')
            ]
        );
    }

    private function getLogEntry(int $id, array $logs): LogEntry
    {
        return current(
            array_filter(
                $logs,
                function ($log) use ($id) {
                    return $log->getId() == $id;
                }
            )
        );
    }

}