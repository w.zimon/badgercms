<?php
declare(strict_types=1);

namespace BadgerCMS\Application\Command\Page;

use BadgerCMS\Domain\Page\Page;

abstract class AbstractPageCommand
{

    protected Page $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function getPage(): Page
    {
        return $this->page;
    }

}