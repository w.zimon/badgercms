<?php

namespace BadgerCMS\Application\Command\PageMenu\CreateMenu;

use BadgerCMS\Domain\Page\Menu;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateMenuCommandTest extends KernelTestCase
{
    private MessageHandlerInterface $handler;
    /**
     * @var \BadgerCMS\Infrastructure\Page\MenuRepository|object|null
     */
    private $repo;


    public function setUp() {
        $kernel = self::bootKernel();
        $this->kernel_root = $kernel->getContainer()->getParameter('kernel.project_dir');
        $this->handler = $kernel->getContainer()->get(CreateMenuHandler::class);
        $this->repo = parent::$container->get('BadgerCMS\Domain\Page\Repository\MenuRepositoryInterface');
    }

    public function testShouldCreateMenu()
    {
        $uuid = Uuid::uuid4();
        $placeholder = 'TEST_MENU';
        $cmd = new CreateMenuCommand(Menu::create($uuid, $placeholder, 'test menu'));
        $this->handler->__invoke($cmd);

        $menuItem = $this->repo->find($uuid);
        $this->assertEquals($placeholder, $menuItem->getPlaceholder());

        $uuid = Uuid::uuid4();
        $cmd = new CreateMenuCommand(Menu::create($uuid, $placeholder. '.2', $placeholder. '.2', '', $menuItem));
        $this->handler->__invoke($cmd);

    }
}
