<?php
declare(strict_types=1);

namespace BadgerCMS\UI\Controller\Admin;

use BadgerCMS\Domain\Page\Menu;
use BadgerCMS\UI\Controller\AbstractAdminController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package BadgerCMS\UI\Controller\Admin
 *
 * @Route("/menu-ajax")
 */
class MenuAjaxSortController extends AbstractAdminController
{

    private EntityManagerInterface $entityManager;
    /**
     * @var \BadgerCMS\Infrastructure\Page\MenuRepository|\Doctrine\Persistence\ObjectRepository
     */
    private $menuRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/sort", options={"expose"=true}, name="admin_menu_ajax_sort")
     */
    public function index(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $menusNestedSetInfo = $this->calc($data);
        $this->menuRepository = $this->entityManager->getRepository(Menu::class);
        $this->update($menusNestedSetInfo);
        return new JsonResponse();
    }

    private function calc(array $items, $lft = 2, $parent = null, $lvl = 1)
    {
        foreach ($items as $k => $item) {
            if (isset($item['entityId']) && $parent == null) {
                $parent = $item['entityId'];
                unset($items[$k]);
            }
        }
        foreach ($items as $k => $item) {
            $items[$k]['parent'] = $parent;
            $items[$k]['lvl'] = $lvl;
            if (isset($item['children']) && count($item['children'])) {
                $items[$k]['lft'] = $lft;
                $items[$k]['children'] = $this->calc($items[$k]['children'], $lft + 1, $items[$k]['id'], $lvl + 1);
                $childrenCount = count($items[$k]['children']);
                $items[$k]['rgt'] = $items[$k]['children'][$childrenCount - 1]['rgt'] + 1;
            } else {
                $items[$k]['lft'] = $lft;
                $items[$k]['children'] = [];
                $items[$k]['rgt'] = $lft + 1;
            }
            $lft += 2;
        }
        return $items;
    }

    private function update($items)
    {
        foreach ($items as $k => $item) {
            $menuItem = $this->menuRepository->findOneBy(['id' => $item['id']]);
            $menuItem->setLft($item['lft']);
            $menuItem->setRgt($item['rgt']);
            $menuItem->setlvl($item['lvl']);
            if ($item['parent'] !== null) {
                $menuItem->setParent($this->entityManager->getReference(Menu::class, $item['parent']));
            }
            $this->entityManager->persist($menuItem);
            $this->entityManager->flush();

            if (isset($item['children']) && count($item['children'])) {
                $this->update($item['children']);
            }
        }
    }

}