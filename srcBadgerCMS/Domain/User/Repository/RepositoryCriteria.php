<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\User\Repository;

use Doctrine\Common\Collections\Criteria;

class RepositoryCriteria
{

    public static function adminListCriteria(): Criteria
    {
        $criteria = Criteria::create();
        $criteria->orderBy(['u.login' => 'ASC']);

        return $criteria;
    }

}