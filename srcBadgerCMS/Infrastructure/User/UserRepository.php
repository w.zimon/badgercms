<?php
declare(strict_types=1);

namespace BadgerCMS\Infrastructure\User;

use BadgerCMS\Domain\User\Exception\UserNotExistException;
use BadgerCMS\Domain\User\Repository\UserRepositoryInterface;
use BadgerCMS\Domain\User\User;
use BadgerCMS\Domain\User\ValueObject\AuthVO;
use Doctrine\DBAL\ParameterType;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserRepository extends AbstractDoctrineRepository implements UserRepositoryInterface
{

    const USER_TABLE = 'users';

    protected function getEntityClassName(): string
    {
        return User::class;
    }

    public function getAuth(string $login): AuthVO
    {
        $qb = $this->createConnectionQueryBuilder();
        $qb->select('id', 'login', 'email', 'password', 'role', 'active', 'blocked', 'removed')
            ->from(self::USER_TABLE)
            ->where($qb->expr()->like('login', ':login'))
            ->setParameter('login', $login, ParameterType::STRING)
            ->andWhere($qb->expr()->eq('removed', ':false'))
            ->setParameter('false', false, ParameterType::BOOLEAN);

        $data = $qb->execute()->fetch();

        if (is_array($data)) {
            $authVO = new AuthVO($data);
        } else {
            throw new UsernameNotFoundException(sprintf('User with login: %s does not exist.', $login));
        }

        return $authVO;
    }

    public function getCurrentUserPassword(UuidInterface $uuid): string
    {
        $qb = $this->createConnectionQueryBuilder();
        $qb->select('password')
            ->from(self::USER_TABLE)
            ->where($qb->expr()->eq('id', ':id'))
            ->setParameter('id', $uuid, ParameterType::STRING);

        $data = $qb->execute()->fetchColumn();

        if($data == null){
            throw new UserNotExistException(sprintf('User with id: %s don\'t exist.', $uuid));
        }

        return $data;
    }

}