<?php
declare(strict_types=1);

namespace BadgerCMS\Domain\User\Repository;

use BadgerCMS\Domain\User\ValueObject\AuthVO;
use Ramsey\Uuid\UuidInterface;

interface UserRepositoryInterface
{

    public function getAuth(string $login): AuthVO;

    public function getCurrentUserPassword(UuidInterface $uuid): string;

}